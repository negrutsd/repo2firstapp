import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {
  isAddingNewPerson:boolean = false;
  people = [];

  constructor() { }

  ngOnInit() {
    this.people = [
      {
        name: 'Negrut Dan',
        status: 'Curently code',
        phone: '0756917726',
        email: 'negrut@gmail.com',
      },
      {
      name: 'Popescu Adrian',
      status: 'Description',
      phone: '0763425634',
      email: 'popescu@gmail.com',
      }
    ];
  }
onSubmit(newPerson){
  this.people.push(newPerson);
}
}
